#include "sha1.h"

#include <stdio.h>
#include <string.h>
#include <openssl/sha.h>

int wolf_makeSha1FromName(const char *nameAndGroup, char *output, size_t bufferSize)
{
    /* Промежуточный буфер для хэша SHA1, будет содержать байт-числа */
    unsigned char digest[SHA_DIGEST_LENGTH];
    /* Длина данных, записанных в выходной буфер */
    size_t len = 0;
    /* Человеко-читаемая строка, содержащая хэш-сумму */
    char mdString[SHA_DIGEST_LENGTH * 2 + 1];

    /* Выполнение SHA1-хэширования через OpenSSL: Входная строка, её длина, и выходной буфер */
    // SHA1((unsigned char*)&nameAndGroup, strlen(nameAndGroup), (unsigned char*)&digest);
    SHA_CTX ctx;
    SHA1_Init(&ctx);
    SHA1_Update(&ctx, nameAndGroup, strlen(nameAndGroup));
    SHA1_Final(digest, &ctx);

    /* Посимвольно преобразуем числа из SHA1-суммы в человекочитаемую строку */
    for(int i = 0; i < SHA_DIGEST_LENGTH; i++)
         sprintf(&mdString[i * 2], "%02x", (unsigned int)digest[i]);

    /* Измеряем её длину и ставим завершающий ноль */
    len = strlen(mdString) + 1;
    if(len + 1 >= bufferSize)
    {
        len = bufferSize;
        mdString[len] = '\0';
    }

    /* Выводим строку в выходной буфер */
    memcpy(output, mdString, len);

    /* Возвращаем её длину */
    return (int)(len - 1);
}
