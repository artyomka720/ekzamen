#ifndef PIKINV_H
#define PIKINV_H

#include <QMainWindow>

namespace Ui {
class PikInv;
}

class PikInv : public QMainWindow
{
    Q_OBJECT

public:
    explicit PikInv(QWidget *parent = nullptr);
    ~PikInv();

private slots:
    /**
     * @brief Кнопка "Захэшировать"
     */
    void on_doSha1_clicked();
    /**
     * @brief Пункт меню "Открыть картинку"
     */
    void on_actionOpenPik_triggered();
    /**
     * @brief Кнопка "Внедрить код в картинку"
     */
    void on_injectHash_clicked();

private:
    Ui::PikInv *ui;
    //! Последний путь к картинке
    QString m_lastPikPath;
};

#endif // PIKINV_H
