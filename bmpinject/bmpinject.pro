#-------------------------------------------------
#
# Project created by QtCreator 2019-06-10T14:25:54
#
#-------------------------------------------------

QT       -= core gui
CONFIG -= qt

DESTDIR = $$PWD/../libs

TARGET = bmpinject
TEMPLATE = lib
CONFIG += staticlib

DEFINES += BMPINJECT_LIBRARY

SOURCES += \
        bmpinject.c

HEADERS += \
        bmpinject.h
