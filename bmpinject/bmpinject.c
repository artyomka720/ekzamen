#include "bmpinject.h"
#include <stdio.h>
#include <string.h>

#if defined(_WIN32)

// Адская необходимость для винды, иначе никак
#include <windows.h>
FILE *my_fopen(const char *filename, const char *mode)
{
    int new_Len1 = 0;
    int new_Len2 = 0;
    int fn_len_s = strlen(filename);
    int m_len_s  = strlen(mode);
    if(fn_len_s==0) return NULL;
    if(m_len_s==0) return NULL;
    wchar_t path[MAX_PATH];
    wchar_t wmode[MAX_PATH];
    /*  Преобразуем путь и режим из UTF8 в UTF16 и используем юникод-функцию открытия файла
        Это позволит нам корректно поддерживать пути на любом языке не зависимо от системной локали */
    new_Len1 = MultiByteToWideChar(CP_UTF8, 0, filename, fn_len_s, path, fn_len_s);
    if(new_Len1>=MAX_PATH) return NULL;
    path[new_Len1] = L'\0';
    new_Len2 = MultiByteToWideChar(CP_UTF8, 0, mode, m_len_s, wmode, m_len_s);
    if(new_Len2>=MAX_PATH) return NULL;
    wmode[new_Len2] = L'\0';
    FILE *f = _wfopen(path, wmode);
    return f;
}

#else

/* Для линухов и макосей, всё хорошо и офигенно */
#define my_fopen fopen

#endif

/* Карта байт, пригодных для хранения своих данных, по варианту 24 */
static const off_t g_dataBytes[] =
{
    3836, 193981, 215728, 95160, 248121, 56483, 224056, 292964, 393860, 312352, 270494,
    409860, 319115, 277532, 298372, 146417, 283453, 83837, 339015, 192225, 210022, 246084,
    135388, 293838, 138057, 252959, 433691, 377057, 415117, 262676, 239747, 286196, 10862,
    116518, 50889, 388878, 377764, 255116, 185888, 263466, 20354, 53321, 71040, 122816,
    261302, 224299, 135582, 54112, 403514, 93917, 222056, 283261, 78505, 406673, 71603,
    73221, 275757, 56339, 208561, 217472, 406668, 337898, 173448, 86730, 44146, 237300,
    135579, 404229, 311444, 307763, 305772, 324817, 352106, 79145, 73794, 136204, 208978,
    247056, 221517, 348342, 350921, 198460, 228564, 169248, 378487, 138763, 291528, 316246,
    304054, 129639, 250247, 255900, 380706, 389646, 422216, 327855, 361086, 330094, 96069,
    384625, 300560, 327219, 275960, 261882, 211671, 23766, 191562, 423984, 95102, 403094,
    56031, 249218, 139585, 397498, 314044, 104694, 256197, 1191, 403511, 377985, 268787,
    257988, 132592, 13066, 338754, 160047, 256617, 151068, 244809, 185479, 371549, 358045,
    20473, 294586, 206537, 396634, 205269, 64850, 299916, 410518, 319962, 104957, 5315,
    86696, 423295, 292925, 304688, 121320, 390187, 210162, 105817, 283172, 83443, 283777,
    85243, 195187, 208861, 79394, 75525, 229581
};

const char *wolf_injectCode(const char *pathToBmp, const char *hash)
{
    /* Открываем BMP-файл в режиме чтения и обновления, т.е. с возможностью перезаписывать любые байты */
    FILE *bmp = my_fopen(pathToBmp, "r+b");
    if(bmp)
    {
        /* Берём размер строки хэша */
        size_t hashLen = strlen(hash);
        size_t i = 0, j = 0;
        const unsigned char ending[2] = {0x00, 0xFF};
        off_t imgOffset = 0; unsigned char lengthBytes[4];

        if(fseek(bmp, 0x0A, SEEK_SET) != 0)
        {
            fclose(bmp);
            return "SEEK ERROR!!!";
        }

        if(fread(lengthBytes, 1, 4, bmp) != 4)
        {
            fclose(bmp);
            return "READ ERROR!!!";
        }

        imgOffset = (off_t)(((unsigned)lengthBytes[0] << 0)  | ((unsigned)lengthBytes[1] << 8) |
                            ((unsigned)lengthBytes[2] << 16) | ((unsigned)lengthBytes[3] << 24));

        /* Побайтово записываем в пригодные байты по карте */
        for(i = 0; i < hashLen; i++)
        {
            /* Установить позицию */
            if(fseek(bmp, imgOffset + g_dataBytes[i], SEEK_SET) != 0)
            {
                fclose(bmp);
                return "SEEK ERROR!!!";
            }

            /* Записать байт */
            if(fwrite(&hash[i], 1, 1, bmp) != 1)
            {
                fclose(bmp);
                return "WRITE ERROR!!!";
            }
        }

        /* И затем записываем завершающий хвост */
        for(j = 0; j < 2; i++, j++)
        {
            /* Установить позицию */
            if(fseek(bmp, imgOffset + g_dataBytes[i], SEEK_SET) != 0)
            {
                fclose(bmp);
                return "SEEK ERROR!!!";
            }

            /* Записать байт */
            if(fwrite(&ending[j], 1, 1, bmp) != 1)
            {
                fclose(bmp);
                return "WRITE ERROR!!!";
            }
        }

        fclose(bmp);
        return NULL;
    }
    return "CAN NOT OPEN THAT DAMNED FILE!!!";
}

int wolf_extractCode(const char *pathToBmp, char *output, size_t outputSize)
{
    /* Открыть файл на чтение */
    FILE *bmp = my_fopen(pathToBmp, "rb");
    if(bmp)
    {
        size_t i = 0;
        off_t imgOffset = 0; unsigned char lengthBytes[4];

        if(fseek(bmp, 0x0A, SEEK_SET) != 0)
        {
            fclose(bmp);
            return -1;
        }

        if(fread(lengthBytes, 1, 4, bmp) != 4)
        {
            fclose(bmp);
            return -1;
        }

        imgOffset = (off_t)(((unsigned)lengthBytes[0] << 0)  | ((unsigned)lengthBytes[1] << 8) |
                            ((unsigned)lengthBytes[2] << 16) | ((unsigned)lengthBytes[3] << 24));

        /* Читаем байты по карте */
        for(i = 0; i < outputSize - 1; i++)
        {
            /* Позиционируемся */
            if(fseek(bmp, imgOffset + g_dataBytes[i], SEEK_SET) != 0)
            {
                fclose(bmp);
                return -1;
            }

            /* Считываем байт */
            if(fread(&output[i], 1, 1, bmp) != 1)
            {
                fclose(bmp);
                return -1;
            }

            /* Если обнаружен хвост - выходим */
            if((unsigned char)output[i] == 0x00 || (unsigned char)output[i] == 0xFF)
                break;
        }
        /* Запишем завершающий ноль */
        output[i] = '\0';
        fclose(bmp);
        /* и вернём длину полученной последовательности */
        return (int)i;
    }

    /* возвоащаем -1 если не получилось открыть файл */
    return -1;
}
